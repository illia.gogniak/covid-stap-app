const today = new Date();


let store = {
    cases: [
        {
            value: 'all',
            label: 'All',
        },
        {
            value: 'confirmed',
            label: 'Confirmed',
        },
        {
            value: 'recovered',
            label: 'Recovered',
        },
        {
            value: 'deaths',
            label: 'Deaths',
        },
    ],
    dateInfo: {
        actual: today.getFullYear() + '-' + String(today.getMonth() + 1).padStart(2, '0') + '-' + String(today.getDate()).padStart(2, '0'),
        from: '2020-01-01'
    }

}

export default store;