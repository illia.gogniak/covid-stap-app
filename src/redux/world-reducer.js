let today = new Date();
today = today.getFullYear() + '-' + String(today.getMonth() + 1).padStart(2, '0') + '-' + String(today.getDate()).padStart(2, '0');

const FROM = 'FROM';
const TO = 'TO';
const CASE = 'CASE';
const DATA = 'DATA';

const initialState = {
    cases: [
        {
            value: 'All',
            label: 'All cases',
        },
        {
            value: 'AllNew',
            label: 'New cases',
        },
        {
            value: 'AllTotal',
            label: 'Total cases',
        },
        {
            value: 'NewConfirmed',
            label: 'Confirmed (new)',
        },
        {
            value: 'NewDeaths',
            label: 'Deaths (new)',
        },
        {
            value: 'NewRecovered',
            label: 'Recovered (new)',
        },
        {
            value: 'TotalConfirmed',
            label: 'Confirmed (total)',
        },
        {
            value: 'TotalDeaths',
            label: 'Deaths (total)',
        },
        {
            value: 'TotalRecovered',
            label: 'Recovered (total)',
        },
    ],
    dateInfo: {
        to: today,
        from: '2021-07-01',
        fromInitial: '2021-07-01',
        case: 'AllNew',
        apiStrings: {
            world: 'https://api.covid19api.com/world?from=2021-07-01T00:00:00Z&to=' + today + 'T00:00:00Z'
        },
        apiData: []
    }
}


const worldReducer = (state = initialState, action) => {
    switch(action.type) {
        case FROM:
            state.dateInfo.from = action.payload;
            state.dateInfo.apiStrings.world = 'https://api.covid19api.com/world?from=' + state.dateInfo.from + 'T00:00:00Z&to=' + state.dateInfo.to + 'T00:00:00Z';
            break;
        case TO:
            state.dateInfo.to = action.payload;
            state.dateInfo.apiStrings.world = 'https://api.covid19api.com/world?from=' + state.dateInfo.from + 'T00:00:00Z&to=' + state.dateInfo.to + 'T00:00:00Z';
            break;
        case CASE:
            state.dateInfo.case = action.payload
            break;
        case DATA:
            state.dateInfo.apiData = action.payload
            break;
        default:
            console.log('Error in the action in countryReducer');
    }

    return state;
}

export const dateFromActionCreator = (date) => {
    return {
        type: FROM,
        payload: date
    }
}

export const dateToActionCreator = (date) => {
    return {
        type: TO,
        payload: date
    }
}

export const chooseCaseActionCreator = (date) => {
    return {
        type: CASE,
        payload: date
    }
}

export const apiDataActionCreator = (date) => {
    return {
        type: DATA,
        payload: date
    }
}

export default worldReducer;