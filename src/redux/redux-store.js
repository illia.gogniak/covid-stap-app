import {applyMiddleware, combineReducers, createStore} from 'redux';
import counterReducer from "./counter-reducer";
import worldReducer from "./world-reducer";
import {composeWithDevTools} from "redux-devtools-extension";
import {logger} from "redux-logger/src";
import countryReducer from "./country-reducer";
import premiumReducer from "./premium-reducer";

let reducers = combineReducers({
    counter: counterReducer,
    world: worldReducer,
    country: countryReducer,
    premium: premiumReducer
});

let store = createStore(
    reducers,
    composeWithDevTools(applyMiddleware(logger))
);

export default store;