
const INCREMENT = 'INCREMENT';
const DECREMENT = 'DECREMENT';

const initialState = {
    counter: 0
}


const counterReducer = (state = initialState, action) => {
    switch(action.type) {
        case INCREMENT:
            state.counter++;
            break;
        case DECREMENT:
            state.counter--
            break;
        default:
            console.log('Error in the action in counterReducer');
    }

    return state;
}

export const incrementActionCreator = () => {
    return {
        type: INCREMENT
    }
}

export const decrementActionCreator = () => {
    return {
        type: DECREMENT
    }
}

export default counterReducer;