import React from 'react';
import TextField from '@material-ui/core/TextField';
import {MenuItem} from "@material-ui/core";
import DatePicker from "../DatePickers/DatePicker";
import {If} from "control-statements";

import styles from './FilterTabs.module.css';

export default function FilterTabs(props) {
    const {cases, dateInfo, country, countries, dateFromActionCreator, dateToActionCreator, chooseCaseActionCreator, countryActionCreator, dispatch} = props;

    const changeFrom = (e) => {
        dispatch(dateFromActionCreator(e.target.value));
    };
    const changeTo = (e) => {
        dispatch(dateToActionCreator(e.target.value));
    };
    const changeCase = (e) => {
        dispatch(chooseCaseActionCreator(e.target.value));
    }
    const changeCountry = (e) => {
        dispatch(countryActionCreator(e.target.value));
    }

    return (
        <div className={styles.wrapper}>
            <DatePicker label="Date from" defaultValue={dateInfo.from} onChange={changeFrom}/>
            <If condition={country && !country}>
                <DatePicker label="Date to" actualDate={dateInfo.to} onChange={changeTo}/>
            </If>
            <If condition={cases && cases}>
            <form noValidate autoComplete="off">
                <TextField
                    id="standard-select-currency"
                    select
                    label="Case type"
                    value={dateInfo.case}
                    onChange={changeCase}
                >
                    {cases && cases.map((option) => (
                        <MenuItem key={option.value} value={option.value}>
                            {option.label}
                        </MenuItem>
                    ))}
                </TextField>
            </form>
            </If>
            <If condition={country && country}>
                <form noValidate autoComplete="off">
                    <TextField
                        id="standard-select-currency"
                        select
                        label="Country"
                        value={dateInfo.country}
                        onChange={changeCountry}
                    >
                        {countries && countries.map((option) => (
                            <MenuItem key={option.Slug} value={option.Slug}>
                                {option.Country}
                            </MenuItem>
                        ))}
                    </TextField>
                </form>
            </If>

        </div>
    );
}