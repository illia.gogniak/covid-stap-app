import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import DashboardIcon from '@material-ui/icons/Dashboard';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import PeopleIcon from '@material-ui/icons/People';
import InfoIcon from '@material-ui/icons/Info';
import AssignmentIcon from '@material-ui/icons/Assignment';
import {NavLink} from "react-router-dom";

import styles from '../App.module.css';


export const mainListItems = (
    <div>
        <ListItem button>
            <NavLink to="/about" className={styles.navLink} activeClassName={styles.navLink__active}>
                <ListItemIcon>
                    <InfoIcon/>
                </ListItemIcon>
                <ListItemText primary="Home"/>
            </NavLink>
        </ListItem>
        <ListItem button>
            <NavLink to="/world" className={styles.navLink} activeClassName={styles.navLink__active}>
                <ListItemIcon>
                    <DashboardIcon/>
                </ListItemIcon>
                <ListItemText primary="World WIP"/>
            </NavLink>
        </ListItem>
        <ListItem button>
            <NavLink to="/country" className={styles.navLink} activeClassName={styles.navLink__active}>
                <ListItemIcon>
                    <ShoppingCartIcon/>
                </ListItemIcon>
                <ListItemText primary="By Country"/>
            </NavLink>
        </ListItem>
        <ListItem button>
            <NavLink to="/live-by-country" className={styles.navLink} activeClassName={styles.navLink__active}>
                <ListItemIcon>
                    <PeopleIcon/>
                </ListItemIcon>
                <ListItemText primary="Live By Country"/>
            </NavLink>
        </ListItem>
    </div>
);

export const secondaryListItems = (
    <div>
        <ListSubheader inset>Saved reports</ListSubheader>
        <ListItem button>
            <ListItemIcon>
                <AssignmentIcon/>
            </ListItemIcon>
            <ListItemText primary="Current month"/>
        </ListItem>
        <ListItem button>
            <ListItemIcon>
                <AssignmentIcon/>
            </ListItemIcon>
            <ListItemText primary="Last quarter"/>
        </ListItem>
        <ListItem button>
            <ListItemIcon>
                <AssignmentIcon/>
            </ListItemIcon>
            <ListItemText primary="Year-end sale"/>
        </ListItem>
    </div>
);