import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import WorldWIP from '../../pages/WorldWIP';
import CountriesTotal from '../../pages/CountriesTotal';
import LiveByCountry from '../../pages/LiveByCountry';
import {Route, Switch} from "react-router-dom";

import dashboardStyles from './config';
import AppBarComponent from "../AppBar/AppBarComponent";
import SideBar from "../SideBar/SideBar";
import ContainerComponent from "../ContainerComponet/ContainerComponent";
import About from "../About/About";

const useStyles = dashboardStyles;

export default function Dashboard(props) {
    const {state, dispatch} = props;

    const classes = useStyles();
    const [open, setOpen] = React.useState(true);


    return (
        <div className={classes.root}>
            <CssBaseline/>
            <AppBarComponent open={open} setState={setOpen}/>
            <SideBar open={open} setState={setOpen}/>
            <main className={classes.content}>
                <div className={classes.appBarSpacer}/>
                <Switch>
                    <Route exact path='/about'>
                        <ContainerComponent state={state} dispatch={props.dispatch}>
                            <About state={state} dispatch={props.dispatch}/>
                        </ContainerComponent>
                    </Route>
                    <Route path='/world'>
                        <ContainerComponent state={state} dispatch={props.dispatch}>
                            <WorldWIP cases={state.world.cases} dateInfo={state.world.dateInfo} dispatch={dispatch}/>
                        </ContainerComponent>
                    </Route>
                    <Route path='/country'>
                        <ContainerComponent state={state} dispatch={props.dispatch}>
                            <CountriesTotal countries={state.country.countries} cases={state.country.cases} dateInfo={state.country.dateInfo} dispatch={props.dispatch} />
                        </ContainerComponent>
                    </Route>
                    <Route path='/live-by-country'>
                        <ContainerComponent state={state} dispatch={props.dispatch}>
                            <LiveByCountry countries={state.premium.countries} dateInfo={state.premium.dateInfo} dispatch={props.dispatch} />
                        </ContainerComponent>
                    </Route>
                </Switch>
            </main>
        </div>
    );
}