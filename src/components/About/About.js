import React from "react";
import {decrementActionCreator, incrementActionCreator} from "../../redux/counter-reducer";



export default function About(props) {
    const {state} = props;
    console.log('About', state);

    let incrementCounter = () => {
        props.dispatch(incrementActionCreator());
    }
    let decrementCounter = () => {
        props.dispatch(decrementActionCreator());
    }

    return (
        <div>
            <p>About page with description</p>
            <div>
                Counter:
                <span id="counter">{state.counter.counter}</span>
            </div>
            <button onClick={incrementCounter} id="add">Add</button>
            <button onClick={decrementCounter} id="sub">Sub</button>
        </div>
    )
};