import TextField from "@material-ui/core/TextField";
import React from "react";

import stylesDatePicker from './config.js';

export default function DatePicker(props) {
    const {label, defaultValue, actualDate, onChange} = props;

    const classes = stylesDatePicker();

    return (
        <form className={classes.container} noValidate>
            <TextField
                id="date"
                label={label}
                type="date"
                defaultValue={defaultValue ? defaultValue : actualDate}
                className={classes.textField}
                InputLabelProps={{
                    shrink: true,
                }}
                onChange={onChange}
            />
        </form>
    );
}