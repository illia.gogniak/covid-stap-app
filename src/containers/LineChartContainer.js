import React from 'react';
import {useTheme} from '@material-ui/core/styles';
import {LineChart, Line, XAxis, YAxis, Label, ResponsiveContainer, Legend, Tooltip} from 'recharts';
import Title from '../components/Title';
import FilterTabs from "../components/FilterTabs/FilterTabs";

export default function LineChartContainer(props) {
    const {
        cases,
        dateInfo,
        lineKeys,
        data,
        dispatch,
        dateFromActionCreator,
        dateToActionCreator,
        chooseCaseActionCreator,
        countryActionCreator,
        chartHeader,
        country,
        countries,
        hasLegend,
    } = props;

    const theme = useTheme();

    const strokeColors = ["#E03384", "#E9CF75", "#3AA46C", "#8884d8", "#82ca9d", "#884567"]


    return (
        <>
            <FilterTabs cases={cases} dateInfo={dateInfo} dispatch={dispatch}
                        country={country} countries={countries}
                        countryActionCreator={countryActionCreator}
                        dateFromActionCreator={dateFromActionCreator} dateToActionCreator={dateToActionCreator}
                        chooseCaseActionCreator={chooseCaseActionCreator}/>

            <Title>{chartHeader}</Title>
            <ResponsiveContainer>
                <LineChart
                    data={dateInfo.apiData}
                    margin={{
                        top: 16,
                        right: 16,
                        bottom: 16,
                        left: 24,
                    }}
                >
                    <XAxis dataKey="Date" stroke={theme.palette.text.secondary} interval={3} angle={30}
                           height={50}>
                        <Label
                            style={{textAnchor: 'middle', fill: theme.palette.text.primary}}
                        >
                            Date
                        </Label>
                    </XAxis>
                    <YAxis stroke={theme.palette.text.secondary}>
                        <Label
                            angle={270}
                            position="left"
                            style={{textAnchor: 'middle', fill: theme.palette.text.primary}}
                        >
                            Amount
                        </Label>
                    </YAxis>
                    <Tooltip/>
                    {hasLegend && <Legend/>}

                    {lineKeys?.map((line, i) => (
                        <Line type="monotone" dataKey={line} stroke={strokeColors[i]}/>
                    ))}

                </LineChart>
            </ResponsiveContainer>
        </>
    );
}