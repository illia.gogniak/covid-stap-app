import React from 'react';
import Link from '@material-ui/core/Link';
import {makeStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Title from '../components/Title';
import FilterTabs from "../components/FilterTabs/FilterTabs";



//TODO: do something with styles
const useStyles = makeStyles((theme) => ({
    seeMore: {
        marginTop: theme.spacing(3),
    },
}));

export default function TableInfoContainer(props) {

    const { dateInfo, countries, dateFromActionCreator, countryActionCreator, showMore, chartHeader, dispatch} = props;

    const classes = useStyles();

    return (
        <>
            <FilterTabs country countries={countries} dateInfo={dateInfo} dispatch={dispatch}
                        dateFromActionCreator={dateFromActionCreator}
                        countryActionCreator={countryActionCreator} />
            <React.Fragment>
                {/*TODO: можно поменять таблицу на более красивую: пагинация вместо кнопки, фильтрация*/}
                <Title>{chartHeader}</Title>
                <Table size="small">
                    <TableHead>
                        <TableRow>
                            <TableCell>Date</TableCell>
                            <TableCell>Active</TableCell>
                            <TableCell>Confirmed</TableCell>
                            <TableCell>Deaths</TableCell>
                            <TableCell>Recovered</TableCell>
                            <TableCell>Lat</TableCell>
                            <TableCell>Lon</TableCell>
                            <TableCell>Province</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {dateInfo.apiData.slice(0, dateInfo.itemsToShow).map((row) => (
                            <TableRow key={row.ID}>
                                <TableCell>{row.Date}</TableCell>
                                <TableCell>{row.Active}</TableCell>
                                <TableCell>{row.Confirmed}</TableCell>
                                <TableCell>{row.Deaths}</TableCell>
                                <TableCell>{row.Recovered}</TableCell>
                                <TableCell>{row.Lat}</TableCell>
                                <TableCell>{row.Lon}</TableCell>
                                <TableCell>{row.Province}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
                <div className={classes.seeMore}>
                    <Link color="primary" href="#" onClick={showMore}>
                        See more
                    </Link>
                </div>
            </React.Fragment>
        </>

    );
}