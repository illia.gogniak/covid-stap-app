import React, {useEffect} from 'react';
import {
    apiDataActionCreator,
    dateFromActionCreator,
    countryActionCreator,
    reduceTableActionCreator
} from "../redux/premium-reducer";
import TableInfoContainer from "../containers/TableInfoContainer";

export default function LiveCountry(props) {

    const {dateInfo, dispatch, countries} = props;

    let data = [];

    useEffect(() => {
        // if (!dateInfo.apiData.length || dateInfo.fromInitial > dateInfo.from || dateInfo.countriesInitial.indexOf(dateInfo.country) === -1 ) {
        //     dateInfo.fromInitial = dateInfo.from;
        //     dateInfo.countriesInitial.push(dateInfo.country);
        //     getData(dateInfo.apiStrings.world, apiDataActionCreator);
        // }
        getData(dateInfo.apiStrings.world, apiDataActionCreator);
        console.log('UseEffect', dateInfo.apiData);
    }, [dateInfo.apiStrings.world])

    const getData = async (apiUrl, cb) => {
        let result = {};
        try {
            const response = await fetch(apiUrl);
            result = await response?.json();
            cb && dispatch(cb(result))
        } catch (e) {
            console.log('Error PREMIUM', e)
        }

        return result;
    }


    //TODO: не работало и выдавло -1. Оказалось, потому что в Date строка с доп подстрокой времени.
    // Почему в предыдущих графиках работало? Скорее всего не работало - просто проверка индекса (забыл об этом).
    // Но а почему на этой странице это не срабатывало??? Потому что на других страницвх в фильтр передавались данные
    // с апи, а не после поиска индекса

    // let index = dateInfo.apiData && dateInfo.apiData.findIndex(el => el.Date === (dateInfo.from + 'T00:00:00Z'));
    // let index = dateInfo.apiData && dateInfo.apiData.findIndex(el => el.Date === dateInfo.from);
    //
    // data = index ? dateInfo.apiData.slice(index) : dateInfo.apiData;
    //
    // const filteredData = (data) => {
    //     data.forEach(e => {
    //         e.Date = e.Date.slice(0, 10);
    //     });
    //     data.sort((a, b) => {
    //         return a.Date < b.Date ? -1 : 1;
    //     });
    // }
    // filteredData(dateInfo.apiData);

    console.log('apiData PREMIUM/COUNTRY', dateInfo.apiData);

    const showMore = () => {
        dispatch(reduceTableActionCreator())
    }

    let chartHeader = dateInfo.countryName;

    return (
        //TODO: может есть смысл вставлять тут FilterTabs? Тогда не нужно будет через контейнер прокидывать actionCreators. Но тогда не будет единого контейнера
        <TableInfoContainer dateInfo={dateInfo} countries={countries} chartHeader={chartHeader}
                            dispatch={dispatch}
                            dateFromActionCreator={dateFromActionCreator}
                            countryActionCreator={countryActionCreator}
                            showMore={showMore} />
    );
}