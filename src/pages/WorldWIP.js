import React, {useEffect} from 'react';
import {useTheme} from '@material-ui/core/styles';
import {
    apiDataActionCreator,
    chooseCaseActionCreator,
    dateFromActionCreator,
    dateToActionCreator
} from "../redux/world-reducer";
import LineChartContainer from "../containers/LineChartContainer";

export default function World(props) {
    const {cases, dateInfo, dispatch} = props;

    const theme = useTheme();

    //TODO: вызывать просто onChange у инпутов всех. И в итоге можно делать провеку условия новых value по сравнению с теми, что записаны в state
    useEffect(() => {
        if (!dateInfo.apiData.length || dateInfo.fromInitial > dateInfo.from) {
            dateInfo.fromInitial = dateInfo.from;
            getData(dateInfo.apiStrings.world, apiDataActionCreator);
        }
        getData(dateInfo.apiStrings.world, apiDataActionCreator);
    }, [dateInfo.apiStrings.world])

    const getData = async (apiUrl, cb) => {
        let result = {};
        try {
            const response = await fetch(apiUrl);
            result = await response?.json();
            cb && dispatch(cb(result))
        } catch (e) {
            console.log('Error WORLD', e)
        }

        return result;
    }

    let index = dateInfo.apiData.findIndex(el => el.Date === dateInfo.from);

    let data = index ? dateInfo.apiData.slice(index) : dateInfo.apiData;

    const filteredData = (data) => {
        data.forEach(e => {
            e.Date = e.Date.slice(0, 10);
        });
        data.sort((a, b) => {
            return a.Date < b.Date ? -1 : 1;
        });
    }
    filteredData(dateInfo.apiData);

    let chartHeader = dateInfo.case;
    let lineKeys = [dateInfo.case];

    if (dateInfo.case === 'All') {
        chartHeader = 'All cases';
        lineKeys = ["NewConfirmed", "NewDeaths", "NewRecovered", "TotalConfirmed", "TotalDeaths", "TotalRecovered"]
    } else if (dateInfo.case === 'AllNew') {
        chartHeader = 'New cases';
        lineKeys = ["NewConfirmed", "NewDeaths", "NewRecovered"]
    } else if (dateInfo.case === 'AllTotal') {
        chartHeader = 'Total cases';
        lineKeys = ["TotalConfirmed", "TotalDeaths", "TotalRecovered"]
    }


    return (
        <LineChartContainer cases={cases} dateInfo={dateInfo} lineKeys={lineKeys} data={data}
                            hasLegend
                            chartHeader={chartHeader}
                            dispatch={dispatch}
                            chooseCaseActionCreator={chooseCaseActionCreator}
                            dateFromActionCreator={dateFromActionCreator} dateToActionCreator={dateToActionCreator}/>
    );
}