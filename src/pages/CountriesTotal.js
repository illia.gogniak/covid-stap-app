import React, {useEffect} from 'react';
import {useTheme} from '@material-ui/core/styles';
import {apiDataActionCreator, countryActionCreator, chooseCaseActionCreator, dateFromActionCreator} from "../redux/country-reducer";
import LineChartContainer from "../containers/LineChartContainer";
import {dateToActionCreator} from "../redux/world-reducer";

export default function Countries(props) {
    const {cases, dateInfo, dispatch, countries} = props;

    const theme = useTheme();

    let data = [];
    let startY = 5;

    useEffect(() => {
        //TODO: refactor conditions
        if (!dateInfo.apiData.length || dateInfo.fromInitial > dateInfo.from || dateInfo.casesInitial.indexOf(dateInfo.case) === -1 || dateInfo.countriesInitial.indexOf(dateInfo.country) === -1 ) {
            dateInfo.fromInitial = dateInfo.from;
            dateInfo.casesInitial.push(dateInfo.case);
            dateInfo.countriesInitial.push(dateInfo.country);
            getData(dateInfo.apiStrings.world, apiDataActionCreator);
        }
        //TODO: add functional to get new array if we already have this data
        getData(dateInfo.apiStrings.world, apiDataActionCreator);
    }, [dateInfo.apiStrings.world])

    const getData = async (apiUrl, cb) => {
        // alert();
        let result = {};
        try {
            const response = await fetch(apiUrl);
            result = await response?.json();
            cb && dispatch(cb(result))
        } catch (e) {
            console.log('Error COUNTRIES', e)
        }

        return result;
    }

    let index = dateInfo.apiData && dateInfo.apiData.findIndex(el => el.Date === dateInfo.from);
    console.log('indexC', index);

    data = index ? dateInfo.apiData.slice(index) : dateInfo.apiData;

    //TODO: now it gets value from Caes. But I want to do it more clearly
    //TODO: не работает, выдает NaN, при этом устанавливается минимальное значение. Интересно
    //TODO: в данном массиве каждый следующий день значение будет больше, так как суммируются случаи. Так что можно просто взять первый элемент

    console.log('dataC', data);
    //TODO: doesn't work after cmnd+R. Maybe because of async getData()
    function getMinY(data) {
        // return data.reduce((min, current) => current?.Cases < min ? current?.Cases : min, data[0].Cases)
    }
    startY = Math.round(getMinY(data) * 0.999);

    const filteredData = (data) => {
        data.forEach(e => {
            e.Date = e.Date.slice(0, 10);
        });
        data.sort((a, b) => {
            return a.Date < b.Date ? -1 : 1;
        });
    }
    filteredData(dateInfo.apiData);

    let yArr = [];
    function changeYData(data) {
        let newValue;
        data.forEach(e => {
            if(e.Cases > 1000 && e.Cases < 1000000) {
                newValue = e.Cases / 1000;
                newValue = newValue + 'k';
                yArr.push(newValue);
            } else {
                newValue = e.Cases / 1000000;
                Math.round(newValue);
                newValue = newValue + 'kk';
                yArr.push(newValue);
            }

        })
    }
    changeYData(data);

    let chartHeader = dateInfo.countryName + ' / ' + dateInfo.case;
    let lineKeys = ['Cases'];

    return (
        <LineChartContainer cases={cases} dateInfo={dateInfo} lineKeys={lineKeys} data={data}
                            chartHeader={chartHeader}
                            country countries={countries}
                            dispatch={dispatch}
                            chooseCaseActionCreator={chooseCaseActionCreator}
                            countryActionCreator={countryActionCreator}
                            dateFromActionCreator={dateFromActionCreator} dateToActionCreator={dateToActionCreator}/>
    );
}