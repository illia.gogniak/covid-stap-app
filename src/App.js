import './App.module.css';
import {BrowserRouter} from 'react-router-dom';
import Dashboard from "./components/Dashboard/Dashboard";
import React from "react";

function App(props) {
    return (
        <BrowserRouter>
            <Dashboard state={props.state} dispatch={props.dispatch}/>
        </BrowserRouter>
    )
}

export default App;
